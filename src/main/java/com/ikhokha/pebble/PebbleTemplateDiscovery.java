package com.ikhokha.pebble;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;

public class PebbleTemplateDiscovery {

    public String evaluateTemplateToString(String templateName) throws IOException {
        Writer writer = new StringWriter();
        PebbleTemplate template = getTemplate(templateName);
        template.evaluate(writer);

        return writer.toString();
    }

	public String evaluateTemplateToString(
        String templateName,
        Map<String, Object> context
    ) throws IOException {
        Writer writer = new StringWriter();
        PebbleTemplate template = getTemplate(templateName);
        template.evaluate(writer, context);
        
		return writer.toString();
    }
    
    private PebbleTemplate getTemplate(String templateName) {
        return new PebbleEngine
            .Builder()
            .build()
            .getTemplate(templateName);
    }
}
