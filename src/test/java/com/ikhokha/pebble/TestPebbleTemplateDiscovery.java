package com.ikhokha.pebble;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.mitchellbosecke.pebble.error.LoaderException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class TestPebbleTemplateDiscovery {

    private static final String LINE_SEPARATOR = System.lineSeparator();
    private PebbleTemplateDiscovery sut = null;

    @BeforeEach
    public void init() {
        sut = createSut();
    }

    @Test
    public void evaluateToString_GivenInvalidTemplate_ThrowsException() throws LoaderException {
        // Arrange
        String templateName = "invalid";

        // Act
        Throwable exception = assertThrows(LoaderException.class, () -> sut.evaluateTemplateToString(templateName));

        // Assert
        assertTrue(exception.getMessage().contains(templateName));
    }

    @Nested
    class WithBasicTemplate {
        @Test
        public void evaluateToString_RendersTemplate() throws IOException {
            // Arrange
            String expectedText = "SUCCESS";
    
            // Act
            String actualText = sut.evaluateTemplateToString("templates/template.basic.txt");
    
            // Assert
            assertEquals(expectedText, actualText);
        }
    
        @Test
        public void evaluateToString_WithContextVariable_RendersTemplate() throws IOException {
            // Arrange
            String name = "Bob";
            String expectedText = "My name is " + name;
            Map<String, Object> context = new HashMap<>();
            context.put("name", name);
    
            // Act
            String actualText = sut.evaluateTemplateToString("templates/template.contextVariable.txt", context);
    
            // Assert
            assertEquals(expectedText, actualText);
        }
    
    }

    @Nested
    class WithInheritance {
        @Test
        public void evaluateToString_ChildInheritTextFromParentTemplate() throws IOException {
            // Arrange
            String templateName = "templates/template.child.peb";
            String expected = "Parent Header"
                + LINE_SEPARATOR + LINE_SEPARATOR
                + "Child Text"
                + LINE_SEPARATOR + LINE_SEPARATOR
                + "Parent Footer";
    
            // Act
            String actual = sut.evaluateTemplateToString(templateName);
    
            // Assert
            assertEquals(expected, actual);
        }

        @Test
        public void evaluateToString_GivenContextVariable_InheritParentTemplateWithContextVariable() throws IOException {
            // Arrange
            String templateName = "templates/template.childWithContext.peb";
            String name = "Allan";
            Map<String, Object> context = new HashMap<>();
            context.put("name", name);
            String expected = "Parent Head is " + name + " "
                + LINE_SEPARATOR + LINE_SEPARATOR 
                + name + " is my father name"
                + LINE_SEPARATOR + LINE_SEPARATOR
                + "Parent Footer";
    
            // Act
            String actual = sut.evaluateTemplateToString(templateName, context);
    
            // Assert
            assertEquals(expected, actual);
        }
    }

    @Nested
    class WithMultipleLayout { 
        @Test
        public void evaluateToString_RendersEmbededTextFromCardTemplate() throws IOException {
            // Arrange
            String templateName = "templates/template.main.peb";
            String expected = "Card content";
    
            // Act
            String actual = sut.evaluateTemplateToString(templateName);
    
            // Assert
            assertTrue(actual.contains(expected));
        }

        @Test
        public void evaluateToString_DynamicallyRendersEmbedCardTemplate() throws IOException {
            // Arrange
            String templateName = "templates/template.dynamic.peb";
            String expected = "Card content";
            Map<String, Object> context = new HashMap<>();
            context.put("EmbedCardTemplate", true);
    
            // Act
            String actual = sut.evaluateTemplateToString(templateName, context);
    
            // Assert
            assertTrue(actual.contains(expected));
        }

        @Test
        public void evaluateToString_GivenContextVariable_DynamicallyRendersEmbededCardTemplate() throws IOException {
            // Arrange
            String templateName = "templates/template.dynamicWithContext.peb";
            String name = "Bob";
            String cardPage = "Card content " + name;
            String mainPage = "Main Page Content " + name;
            Map<String, Object> context = new HashMap<>();
            context.put("EmbedCardTemplate", true);
            context.put("name", name);
    
            // Act
            String actual = sut.evaluateTemplateToString(templateName, context);
    
            // Assert
            assertTrue(actual.contains(cardPage));
            assertTrue(actual.contains(mainPage));
        }
    }

    private PebbleTemplateDiscovery createSut() {
        return new PebbleTemplateDiscovery();
    }
}
