# Pebble Template Discovery

This is a Maven Java project so you going to need maven installed inorder to run the app

## Install packages and build
* Execute 'mvn package' from command inside root directory
* Or you can use your favourite IDE

## Run unit tests
* Execute 'mvn test' from command inside root directory
* Or use your favourite IDE